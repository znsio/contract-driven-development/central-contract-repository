## Central Contract Repository Demo

Example of a [Central Contract Repository](https://specmatic.in/#contract-as-code) with pre-merge checks to valid API spec standards with [Spectral](https://stoplight.io/open-source/spectral) and [backward compatibility](https://specmatic.in/documentation/backward_compatibility.html) validation with [Specmatic](https://specmatic.in/).
Both linter and backward compatibilty stages have to succeed in order for the merge button on the MR to be enabled.

### Spectral linter stage

We have setup the standard OAS ruleset along with elevating the severity level of `no trailing slash in path` to `error`.

### Specmatic backward compatibility testing

Specmatic backward compatibility pre-merge check compares the Merge Request branch with the main branch.
However Gitlab CI by default only clones the branch for the Merge Request.
So in the `before_script` section in `compatibility` stage we expliclty checkout main branch also.
